
interface RootState {

  isMobileDevice?: boolean

  showPurchaseCouponDialog?: boolean
  showPurchaseCouponSuccessDialog?: boolean
  showActivateCouponDialog?: boolean
  showActivateCouponSuccessDialog?: boolean

  showServiceDetailsDialog?: boolean

  paypalClientID?: string
  paypalSandboxAccount?: string
  paypalSecret?: string
}

export {
  RootState
}
