import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { RootState } from './types'
import mutations from './mutations'

// import account from './account'

// import isMobile from 'ismobilejs'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
  state: {

    // isMobileDevice: isMobile().any,

    showPurchaseCouponDialog: false,
    showPurchaseCouponSuccessDialog: false,
    showActivateCouponDialog: false,
    showActivateCouponSuccessDialog: false,

    showServiceDetailsDialog: false,

    paypalClientID: '',
    paypalSandboxAccount: 'sb-uvkbx801026@business.example.com',
    paypalSecret: 'ECNDWOYFQiVdkty0mUXxkyL10SAxjV8uMHbeq9hXNLq-SO_cGM4TkVY9VH8TTsbGiY_ohOp73rFoMlxs'
  },
  mutations,
  modules: {
    // account
  }
}

export default new Vuex.Store<RootState>(store)
