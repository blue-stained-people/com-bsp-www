import { MutationTree } from 'vuex'
import { RootState } from './types'

const mutations: MutationTree<RootState> = {
  toggleShowPurchaseCouponDialog (state, isOpen: boolean | undefined) {
    const isCurrentlyOpen = state.showPurchaseCouponDialog
    state.showPurchaseCouponDialog = isOpen === undefined ? !isCurrentlyOpen : isOpen
  },

  toggleShowPurchaseCouponSuccessDialog (state, isOpen: boolean | undefined) {
    const isCurrentlyOpen = state.showPurchaseCouponSuccessDialog
    state.showPurchaseCouponSuccessDialog = isOpen === undefined ? !isCurrentlyOpen : isOpen
  },

  toggleShowActivateCouponDialog (state, isOpen: boolean | undefined) {
    const isCurrentlyOpen = state.showActivateCouponDialog
    state.showActivateCouponDialog = isOpen === undefined ? !isCurrentlyOpen : isOpen
  },

  toggleShowActivateCouponSuccessDialog (state, isOpen: boolean | undefined) {
    const isCurrentlyOpen = state.showActivateCouponSuccessDialog
    state.showActivateCouponSuccessDialog = isOpen === undefined ? !isCurrentlyOpen : isOpen
  },

  toggleShowServiceDetailsDialog (state, isOpen: boolean | undefined) {
    const isCurrentlyOpen = state.showServiceDetailsDialog
    state.showServiceDetailsDialog = isOpen === undefined ? !isCurrentlyOpen : isOpen
  }
}

export default mutations
